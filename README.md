#My firefox theme

##Installation

* Execute this script
```shell
git clone https://github.com/lazyfox81/firefox.git
cd .mozilla/firefox/*profile*/
mkdir chrome && cd chrome
ln -s /home/lazyfox/firefox/userChrome.css userChrome.css 
```
* Install [Classic Theme Restorer](https://addons.mozilla.org/en-US/firefox/addon/classicthemerestorer/)
* Export [CTRpreferences.txt](https://raw.githubusercontent.com/lazyfox81/firefox/master/CTRpreferences.txt) to Classic Theme Restorer setting
